package com.alwan.restmahasiswa.dto;

import lombok.Data;

@Data
public class DosenDto {
    private String nik;
    private String nama;
    private String j_kelamin;
}

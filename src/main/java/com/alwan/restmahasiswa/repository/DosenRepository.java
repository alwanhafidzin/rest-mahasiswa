package com.alwan.restmahasiswa.repository;

import com.alwan.restmahasiswa.model.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DosenRepository extends JpaRepository<Dosen, Long> {
}

package com.alwan.restmahasiswa.repository;

import com.alwan.restmahasiswa.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Long> {
}

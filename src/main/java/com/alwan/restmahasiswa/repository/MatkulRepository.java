package com.alwan.restmahasiswa.repository;

import com.alwan.restmahasiswa.model.Matkul;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatkulRepository extends JpaRepository<Matkul, Long> {

}

package com.alwan.restmahasiswa.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNativePakeJoin", entities = {
        @EntityResult( entityClass = CustomMappingModel.class, fields = {
                @FieldResult(name = "nim", column = "m.nim"),
                @FieldResult(name = "nama", column = "m.nama"),
                @FieldResult(name = "nama_dosen", column = "nama_dosen"),
                @FieldResult(name = "matkul", column = "tm.matkul"),
        })
})
public class CustomMappingModel {
    @Id
    private String nim;
    private String nama;
    private String nama_dosen;
    private String matkul;
}

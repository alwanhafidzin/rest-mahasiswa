package com.alwan.restmahasiswa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_mahasiswa")
public class Mahasiswa {
    @Id
    private String nim;
    private String nama;
    private String jurusan;
    private String j_kelamin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_nik")
    @JsonBackReference
    private Dosen dosen;

}

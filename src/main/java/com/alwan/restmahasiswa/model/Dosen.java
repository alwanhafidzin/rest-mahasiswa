package com.alwan.restmahasiswa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_dosen")
@NoArgsConstructor
public class Dosen {
    @Id
    private String nip;
    private String nama;
    private String j_kelamin;

    @OneToOne
    @JoinColumn(name = "id_matkul")
    private Matkul matkul;

    @OneToMany(mappedBy = "dosen", fetch = FetchType.LAZY)
    private List<Mahasiswa> mahasiswa;

    public Dosen(String nip){
        this.nip = nip;
    }
}

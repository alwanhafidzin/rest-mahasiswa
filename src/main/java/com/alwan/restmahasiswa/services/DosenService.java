package com.alwan.restmahasiswa.services;

import com.alwan.restmahasiswa.dto.DosenDto;
import com.alwan.restmahasiswa.model.Dosen;
import com.alwan.restmahasiswa.repository.DosenRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DosenService {

    @Autowired
    private DosenRepository dosenRepository;

    public List<DosenDto> getDosenDto(){
        List<Dosen> dosens = dosenRepository.findAll();
        ModelMapper mm = new ModelMapper();

        return dosens.stream().map(x -> mm.map(x, DosenDto.class)).collect(Collectors.toList());
    }
}

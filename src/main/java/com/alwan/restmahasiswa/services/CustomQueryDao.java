package com.alwan.restmahasiswa.services;
import com.alwan.restmahasiswa.model.CustomMappingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQueryDao {

    @Autowired
    private EntityManager em;

    public List<CustomMappingModel> getCustomQueryNative(String nim){
        String nativeQueryScript = "SELECT m.nim,m.nama,d.nama as nama_dosen,tm.matkul FROM tbl_mahasiswa" +
                " m INNER JOIN tbl_dosen d ON m.dosen_nip = d.nip INNER JOIN tbl_matkul" +
                " tm ON tm.id = d.id_matkul WHERE m.nim = :nim";
        Query q = em.createNativeQuery(nativeQueryScript, "QueryNativePakeJoin");

        List<CustomMappingModel> list = q.setParameter("nim",nim).getResultList();
        return list;
    }
}

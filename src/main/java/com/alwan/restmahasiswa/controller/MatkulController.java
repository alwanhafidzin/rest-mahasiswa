package com.alwan.restmahasiswa.controller;
import com.alwan.restmahasiswa.model.Matkul;
import com.alwan.restmahasiswa.repository.MatkulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "matkul")
public class MatkulController {

    @Autowired
    private MatkulRepository matkulRepository;

    @GetMapping("/getMatkul")
    public ResponseEntity getMatkul(){
        List<Matkul> result = matkulRepository.findAll();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/saveMatkul")
    public ResponseEntity<Matkul> saveMatkul(@RequestBody Matkul matkul){
        Matkul m = matkulRepository.save(matkul);
        return  new ResponseEntity<>(m, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity getMapelById(@PathVariable long id){
        Optional<Matkul> result = matkulRepository.findById(id);
        return  ResponseEntity.ok(result);
    }

}

package com.alwan.restmahasiswa.controller;

import com.alwan.restmahasiswa.dto.DosenDto;
import com.alwan.restmahasiswa.model.Dosen;
import com.alwan.restmahasiswa.repository.DosenRepository;
import com.alwan.restmahasiswa.services.DosenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/dosen")
public class DosenController {

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private DosenService dosenService;

    @GetMapping("/getDosen")
    public ResponseEntity getDosen(){
        List<Dosen> result = dosenRepository.findAll();
        if (result.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(result);
    }

    @PostMapping("/saveDosen")
    public ResponseEntity<Dosen> saveDosen(@RequestBody Dosen dosen){
        Dosen d = dosenRepository.save(dosen);
        return  new ResponseEntity<>(d, HttpStatus.CREATED);
    }

    @GetMapping("/getDosenDto")
    public ResponseEntity getDosenDto() {
        List<DosenDto> dosenDtos = dosenService.getDosenDto();
        return  ResponseEntity.ok(dosenDtos);
    }

}

package com.alwan.restmahasiswa.controller;
import com.alwan.restmahasiswa.model.CustomMappingModel;
import com.alwan.restmahasiswa.model.Mahasiswa;
import com.alwan.restmahasiswa.repository.MahasiswaRepository;
import com.alwan.restmahasiswa.services.CustomQueryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "mahasiswa")
public class MahasiswaController {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private CustomQueryDao customQueryDao;

    @GetMapping("/getMahasiswa")
    public ResponseEntity getDosen(){
        List<Mahasiswa> result = mahasiswaRepository.findAll();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/saveDosen")
    public ResponseEntity<Mahasiswa> saveDosen(@RequestBody Mahasiswa mahasiswa){
        Mahasiswa m = mahasiswaRepository.save(mahasiswa);
        return new ResponseEntity<>(m, HttpStatus.CREATED);
    }

    @GetMapping("/nativeQuery")
    public ResponseEntity<List<CustomMappingModel>> getNativeQuery(@RequestParam String nim){
        List<CustomMappingModel> list = customQueryDao.getCustomQueryNative(nim);
        return ResponseEntity.ok(list);
    }
}

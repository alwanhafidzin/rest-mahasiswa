package com.alwan.restmahasiswa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestMahasiswaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestMahasiswaApplication.class, args);
	}

}
